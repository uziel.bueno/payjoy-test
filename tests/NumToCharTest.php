<?php

use Uziel\NumToChar;
use PHPUnit\Framework\TestCase;

class NumToCharTest extends TestCase
{
    /** @test **/
    function returns_empty_string_when_no_input_is_passed()
    {
        $numToChar = new NumToChar;
        $this->assertEquals('', $numToChar->parse(''));
    }

    /** @test */
    function test_1()
    {
        $numToChar = new NumToChar;
        $this->assertEquals('a', $numToChar->parse('2'));
    }

    /** @test */
    function test_2()
    {
        $numToChar = new NumToChar;
        $this->assertEquals('hi', $numToChar->parse('44 444'));
    }

    /** @test */
    function test_3()
    {
        $numToChar = new NumToChar;
        $this->assertEquals('yes', $numToChar->parse('999337777'));
    }

    /** @test */
    function test_4()
    {
        $numToChar = new NumToChar;
        $this->assertEquals('abcdefghijklmno', $numToChar->parse('2 22 2223 33 3334 44 4445 55 5556 66 666'));
    }
}
