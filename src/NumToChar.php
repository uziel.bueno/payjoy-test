<?php

namespace Uziel;

class NumToChar
{
    private $mappings = [
        '2' => 'a', '22' => 'b', '222' => 'c',
        '3' => 'd', '33' => 'e', '333' => 'f',
        '4' => 'g', '44' => 'h', '444' => 'i',
        '5' => 'j', '55' => 'k', '555' => 'l',
        '6' => 'm', '66' => 'n', '666' => 'o',
        '7' => 'p', '77' => 'q', '777' => 'r', '7777' => 's',
        '8' => 't', '88' => 'u', '888' => 'v',
        '9' => 'w', '99' => 'x', '999' => 'y', '9999' => 'z',
    ];

    public function parse($sequence)
    {
        $digits = str_split($sequence);

        $newSequence = [];
        $currentSequence = '';

        foreach ($digits as $digit) {
            if ($digit !== substr($currentSequence, -1)) {
                $newSequence[] = $currentSequence;
                $currentSequence = $digit;
            } else {
                $currentSequence .= $digit;
            }
        }

        $newSequence[] = $currentSequence;

        $output = '';
        foreach ($newSequence as $key) {
            $output .= array_key_exists($key, $this->mappings) ? $this->mappings[$key] : '';
        }
        return $output;
    }
}
